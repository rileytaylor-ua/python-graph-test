import shelve

import requests
from adal import AuthenticationContext

from config.settings import APP_DOMAIN_ID, CLIENT_ID, TESTOU_APP_DOMAIN_ID, TESTOU_CLIENT_ID, TESTOU_CLIENT_SECRET


def get_session():
    """Create a requests Session object with our stored GraphAPI accessToken"""
    # See if we have a cached token
    with shelve.open('cache.db') as db:
        accessToken = db.get('accessToken', None)
        refreshToken = db.get('refreshToken', None)

    # Create the session
    session = requests.Session()
    session.headers.update({
        'Authorization': f'Bearer {accessToken}',
        'SdkVersion': 'python-graph-test',
        'x-client-SKU': 'python-graph-test',
        'Prefer': 'outlook.timezone="US/Phoenix"'
    })

    # Verify the session and token are still valid
    try:
        r = session.get('https://graph.microsoft.com/v1.0/me')
        r.raise_for_status()
        return session
    except requests.exceptions.HTTPError:
        # The token isn't valid anymore, we need to re-auth it with the refreshToken
        if refreshToken:
            print('Refreshing auth with refresh token')
            token_response = refresh_auth(refreshToken)
            # Update the session
            session.headers.update({
                'Authorization': f'Bearer {token_response["accessToken"]}'
            })
            # Store new tokens for later use
            with shelve.open('cache.db') as db:
                db['accessToken'] = token_response['accessToken']
                db['refreshToken'] = token_response['refreshToken']
            return session
        else:
            print('No existing tokens. Please login with the `login` command.')
            return None
    except requests.exceptions.ConnectionError as ec:
        print("Error Connecting: ", ec)
    except requests.exceptions.Timeout as et:
        print("Timeout Error: ", et)
    except requests.exceptions.RequestException as er:
        print("Error: ", er)

def auth():
    """Login to the Graph API via a user and store the access token for later use."""
    # Authenticate
    ctx = AuthenticationContext(f'https://login.microsoftonline.com/{APP_DOMAIN_ID}/', api_version=None)
    device_code = ctx.acquire_user_code('https://graph.microsoft.com', CLIENT_ID)
    # User has to auth the app first, then re-run the command
    print(device_code['message'])
    # Get the code once approved
    token_response = ctx.acquire_token_with_device_code(
        'https://graph.microsoft.com',
        device_code,
        CLIENT_ID
    )
    # Exit if there is no token
    if not token_response.get('accessToken', None):
        return None

    # Store token for later use
    with shelve.open('cache.db') as db:
        db['accessToken'] = token_response['accessToken']
        db['refreshToken'] = token_response['refreshToken']

    print('Login successful!')

def refresh_auth(refresh_token):
    """Refresh the authentication with the refresh token"""
    # Authenticate
    ctx = AuthenticationContext(f'https://login.microsoftonline.com/{APP_DOMAIN_ID}/', api_version=None)
    # Get a new auth token with the refresh token
    try:
        token_response = ctx.acquire_token_with_refresh_token(
            refresh_token,
            CLIENT_ID,
            'https://graph.microsoft.com'
        )
        return token_response
    except Exception as e:
        print(e)
        print('Error: Unable to acquire new token with refresh token, please use the `login` command again to re-authorize.')

def get_session_as_app():
    """
    Get the session as an application (i.e. like a backend service)
    see: https://docs.microsoft.com/en-us/graph/auth-v2-service
    """
    # See if we have a cached token
    with shelve.open('cache.db') as db:
        accessToken = db.get('backendAccessToken', None)

    # Create the session
    session = requests.Session()
    session.headers.update({
        'SdkVersion': 'python-graph-test',
        'x-client-SKU': 'python-graph-test',
        'Prefer': 'outlook.timezone="US/Phoenix"',
        'Host': 'login.microsoftonline.com'
    })

    if accessToken:
        # Add the access token to the session
        session.headers.update({
            'Authorization': f'Bearer {accessToken}',
        })
        # Verify the session and token are still valid
        try:
            r = session.get('https://graph.microsoft.com/v1.0/')
            r.raise_for_status()
            return session
        except requests.exceptions.HTTPError:
            # Access token expired, lets get a new one
            print('Access Token expired. Attempting to obtain a new one.')
            result = get_access_token(session)
            return result
        except requests.exceptions.ConnectionError as ec:
            print("Error Connecting: ", ec)
        except requests.exceptions.Timeout as et:
            print("Timeout Error: ", et)
        except requests.exceptions.RequestException as er:
            print("Error: ", er)
    else:
        # No access token, let's get it!
        result = get_access_token(session)
        return result

def get_access_token(session):
    try:
        body = {
            'client_id': TESTOU_CLIENT_ID,
            'scope': 'https://graph.microsoft.com/.default',
            'client_secret': TESTOU_CLIENT_SECRET,
            'grant_type': 'client_credentials'
        }
        r = session.get(
            f'https://login.microsoftonline.com/{TESTOU_APP_DOMAIN_ID}/oauth2/v2.0/token',
            headers={
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data=body
        )
        r.raise_for_status()
        data = r.json()

        # Add the access token to the session
        session.headers.update({
            'Authorization': f'Bearer {data["access_token"]}',
        })

        # Store token for later use
        with shelve.open('cache.db') as db:
            db['backendAccessToken'] = data['access_token']

        return session
    except requests.exceptions.HTTPError as he:
        print('Unable to get Access token! Something is horribly wrong.')
        print('HTTP Error: ', he)
    except requests.exceptions.ConnectionError as ec:
        print("Error Connecting: ", ec)
    except requests.exceptions.Timeout as et:
        print("Timeout Error: ", et)
    except requests.exceptions.RequestException as er:
        print("Error: ", er)