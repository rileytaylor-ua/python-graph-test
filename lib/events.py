"""Functions related to the Graph API events endpoint"""

import requests

def create_event(session, calendar_id, event, user=None):
    """
    Create an event for the specified calendar
    returns: (object) an event object
    """
    if user:
        path = f'users/{user}'
    else:
        path = 'me'

    try:
        r = session.post(
            f'https://graph.microsoft.com/v1.0/{path}/calendars/{calendar_id}/events',
            json=event
        )
        r.raise_for_status()
        data = r.json()
        return data
    except requests.exceptions.HTTPError as eh:
        print("Http Error: ", eh)
    except requests.exceptions.ConnectionError as ec:
        print("Error Connecting: ", ec)
    except requests.exceptions.Timeout as et:
        print("Timeout Error: ", et)
    except requests.exceptions.RequestException as er:
        print("Error: ", er)

def update_event(session, calendar, event):
    pass

def delete_event(session, calendar, event):
    pass