import json

import click
import requests

from lib.users import running_as_user
from lib.calendars import verify_or_create_calendar
from lib.auth import auth, get_session, get_session_as_app
from lib.events import create_event, update_event, delete_event


@click.group()
@click.pass_context
def cli(ctx):
    session = get_session()

    ctx.obj = {
        'session': session
    }

@click.command('login')
def login():
    """Login to the API as a user to use the /me endpoints."""
    auth()

@cli.command('create')
@click.option('-c',
              '--calendar',
              required=True,
              type=str,
              help='The calendar to send events to'
)
@click.option('-u',
              '--user',
              required=False,
              type=str,
              help='A user id or principalName'
)
@click.argument('event', type=click.File('rb'))
@click.pass_obj
def create(context, calendar, event, user):
    """Create an event"""
    if context['session']:
        # print who is doing the thing
        running_as_user(context['session'])
        # Read the json from the file
        event_data = json.loads(event.read())
        # Create the calendar (if it doesn't exist) and return its id
        calendar_id = verify_or_create_calendar(context['session'], calendar)
        # Create the event
        created_event = create_event(context['session'], calendar_id, event_data)
        print(f'Event created: {created_event["webLink"]}')

@click.command('update')
@click.pass_obj
def update(context):
    """Update an event"""
    if context['session']:
        running_as_user(context['session'])

@click.command('delete')
@click.pass_obj
def delete(context):
    """Delete an event"""
    if context['session']:
        running_as_user(context['session'])

cli.add_command(login)
cli.add_command(create)
cli.add_command(update)
cli.add_command(delete)

if __name__ == "__main__":
    cli()